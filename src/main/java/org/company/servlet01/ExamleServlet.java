package org.company.servlet01;

import org.company.servlet01.User;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

@WebServlet(urlPatterns = {"/","/examle"}, name = "ExamleServlet")
public class ExamleServlet  extends HttpServlet {

    public ExamleServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getServletPath().equals("/")) {
        /*resp.getOutputStream().write("Hello Elina!".getBytes());
        resp.getOutputStream().flush();*/
            List<User> users = List.of(
                    new User("1", "vasya"),
                    new User("2", "petya")
            );
            req.setAttribute("users", users);
            getServletContext().getRequestDispatcher("/WEB-INF/views/main.jsp").forward(req, resp);

        } else {
            super.doGet(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        ServletOutputStream outputStream = resp.getOutputStream();
        outputStream.print("Good morning "+req.getParameter("vasia"));
        outputStream.flush();
        /*try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
      //  resp.sendRedirect(req.getContextPath()+"/");

    }


}
