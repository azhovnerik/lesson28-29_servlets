package org.company.servlet01;

import lombok.*;
//@AllArgsConstructor

//@RequiredArgsConstructor



//@Builder

@Setter
//@Getter
public class User {

    private   String id;
    private  String login;
    private   String date_born;
    private  String password;

    public User(String id, String login) {
        this.id = id;
        this.login = login;
    }


    public String getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getDate_born() {
        return date_born;
    }

    public String getPassword() {
        return password;
    }
}
